#include <iostream>
#include <vector>
#include <string>
#include <map>

#include "opencv2\core\core.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\contrib\contrib.hpp"

typedef std::vector<std::string> Strings;

typedef std::map<int, std::string> Names;
typedef std::vector<cv::Mat> Images;
typedef std::vector<int> Labels;

typedef cv::Ptr<cv::FaceRecognizer> Model;

static void trainOn(const std::string &dir, Model &model, Names &names);

static void recognizeFor(Model &model, Names &names, const std::string &filename);

static void loadImages(const std::string &dir, Images &images, Labels &labels, Names &names);

static void allFilesIn(const std::string &dir, Strings &list);

static void extractFrom(const std::string &filename, int &label, std::string &name);

static std::string subStrOf(const std::string &s, size_t i, size_t j);
static size_t smaller(size_t a, size_t b);
static size_t bigger(size_t a, size_t b);
static size_t clamp(size_t v, size_t a, size_t b);

int main(int argc, char *argv[]) {
	Model model = cv::createLBPHFaceRecognizer();
	Names names;

	std::string dir = "..\\data\\att";
	trainOn(dir, model, names);

	recognizeFor(model, names, "..\\data\\att\\0-Alice\\10.pgm");
	recognizeFor(model, names, "..\\data\\att\\2-Lizhai Li\\1.png");

	std::cerr << "press any key to quit" << std::endl;

	cv::waitKey();

	return 0;
}

void trainOn(const std::string &dir, Model &model, Names &names) {
	Images images;
	Labels labels;

	loadImages(dir, images, labels, names);

	size_t n = images.size();
	if (n <= 0) {
		std::cerr << "Sample is not enough for training" << std::endl;

		return;
	}

	model->update(images, labels);
}

void recognizeFor(Model &model, Names &names, const std::string &filename) {
	cv::Mat sample = cv::imread(filename, 0);

	int label = -1;
	double confidence = 0.0;

	model->predict(sample, label, confidence);

	std::string name = names[label];
	std::cout << label << ": " << name << " with " << confidence << std::endl;

	cv::imshow(cv::format("%d-%s %f", label, name, confidence), sample);
}

void loadImages(const std::string &dir, Images &images, Labels &labels, Names &names) {
	Strings list;
	allFilesIn(dir, list);

	size_t n = list.size();

	for (size_t i = 0; i < n; i++) {
		std::string filename = list.at(i);

		cv::Mat image = cv::imread(filename, 0);
		if (image.empty()) {
			std::cerr << filename << ": invalid image file" << std::endl;
			continue;
		}

		images.push_back(image);

		int label = -1;
		std::string name = "?";

		extractFrom(filename, label, name);

		labels.push_back(label);
		names[label] = name;

		cv::imshow(dir, image);
		std::cerr << i << "\t" << filename << "\t" << label << "\t" << name << "\t" << std::endl;
	}
}

void allFilesIn(const std::string &dir, Strings &list) {
	list.push_back(cv::format("%s\\%s", dir, "0-Alice\\1.pgm"));
	list.push_back(cv::format("%s\\%s", dir, "0-Alice\\2.pgm"));

	list.push_back(cv::format("%s\\%s", dir, "1-Bob\\1.pgm"));
	list.push_back(cv::format("%s\\%s", dir, "1-Bob\\2.pgm"));

	list.push_back(cv::format("%s\\%s", dir, "2-Lizhai Li\\0.png"));
}

void extractFrom(const std::string &filename, int &label, std::string &name) {
	const char SEP = '\\';

	size_t posLastSep = filename.find_last_of(SEP);
	size_t posSecondSep = filename.find_last_of(SEP, posLastSep-1);

	std::string s = subStrOf(filename, posSecondSep + 1, posLastSep - 1);

	const char DASH = '-';

	size_t posDash = s.find_first_of(DASH);

	std::string labelStr = subStrOf(s, 0, posDash - 1);
	label = atoi(labelStr.c_str());

	name = subStrOf(s, posDash + 1, s.size() - 1);
}

std::string subStrOf(const std::string &s, size_t i, size_t j) {
	size_t n = s.size();

	size_t l = smaller(i, j);
	size_t r = bigger(i, j);

	l = clamp(l, 0, n - 1);
	r = clamp(r, 0, n - 1);

	return s.substr(l, r - l + 1);
}

size_t smaller(size_t a, size_t b) {
	if (a < b) {
		return a;
	}

	return b;
}

size_t bigger(size_t a, size_t b) {
	if (a > b) {
		return a;
	}

	return b;
}

size_t clamp(size_t v, size_t a, size_t b) {
	size_t min = smaller(a, b);
	size_t max = bigger(a, b);

	if (v < a) {
		return a;
	}

	if (v > b) {
		return b;
	}

	return v;
}